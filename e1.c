#include <stdio.h>

int main(void){

int *pPuntero;
int var = 10;
pPuntero = &var;

printf("Before ===> El valor de var es: [%d]\n", var);

printf("Before ===> El valor de pPuntero es: [%d]\n", *pPuntero);

var=20;

printf("After ===> El valor de var es: [%d]\n", var);
printf("After ===> El valor de pPuntero es: [%d]\n", *pPuntero);

return 0;
}
